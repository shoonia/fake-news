/* eslint no-console: off */
const fs = require('fs');

const resetColor = '\x1b[0m';
const redColor = '\x1b[31m';
const greenColor = '\x1b[32m';

fs.copyFile('./build/index.html', './build/404.html', (error) => {
  if (error) {
    console.error(
      redColor,
      'index.html could not be copied to 404.html',
      resetColor,
    );
    throw error;
  }
  console.info(
    greenColor,
    'index.html was copied to 404.html',
    resetColor,
  );
});
