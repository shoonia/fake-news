const getUrl = path => `https://jsonplaceholder.typicode.com${path}`;

export const getRequest = url => fetch(url).then(response => response.json());

export const fetchUsers = () => getRequest(getUrl`/users`);

export const fetchPostsToPage = index => getRequest(
  getUrl(`/posts?_page=${index}&_limit=10`),
);
