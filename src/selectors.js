/* user page */
export const getUsers = state => state.users.items;
export const isUsersFetched = state => state.users.isFetched;
// own props:
export const getParamsId = ownProps => ownProps.match.params.id;
export const getParamsIndex = ownProps => ownProps.match.params.index;

/* profile page */
export const getProfile = state => state.profile.item;
export const getError = state => state.profile.error;
export const isUserLoaded = state => state.profile.isLoaded;

export const getCurrentPosts = state => state.posts.currentPosts;
export const getPageList = state => state.posts.pageList;
