import { spawn, all } from 'redux-saga/effects';

import usersSaga from './users';
import profileSaga from './profile';
import postsSaga from './posts';

export default function* () {
  yield all([
    spawn(usersSaga),
    spawn(profileSaga),
    spawn(postsSaga),
  ]);
}
