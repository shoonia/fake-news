import {
  takeLatest,
  select,
  call,
  put,
} from 'redux-saga/effects';

import { SET_CURRENT_POSTS } from '../constants';
import { getPageList } from '../selectors';
import { putCurrentPosts, addPostsToPageList } from '../actions/posts';
import { fetchPostsToPage } from '../api';

export function* setCurrentPostsSaga({ index }) {
  const pageList = yield select(getPageList);
  if (index in pageList) {
    yield put(putCurrentPosts(pageList[index]));
  } else {
    const posts = yield call(fetchPostsToPage, index);
    yield put(addPostsToPageList(index, posts));
  }
}

export default function* () {
  yield takeLatest(SET_CURRENT_POSTS, setCurrentPostsSaga);
}
