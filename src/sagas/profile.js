import {
  put,
  call,
  select,
  takeLatest,
} from 'redux-saga/effects';

import { PROFILE_PAGE_MOUNT } from '../constants';
import { getUsers, isUsersFetched } from '../selectors';
import { receiveCurrentUser, profileError } from '../actions/profile';
import { fetchUsersSaga } from './users';

export function* getUserByIdSaga(id) {
  const users = yield select(getUsers);
  const currentUser = users.find(user => user.id === id);
  if (currentUser) {
    yield put(receiveCurrentUser(currentUser));
  } else {
    yield put(profileError('404: Not Found'));
  }
}

export function* profilePageMountSaga({ id }) {
  const isFetched = yield select(isUsersFetched);
  if (!isFetched) {
    yield call(fetchUsersSaga);
  }
  yield call(getUserByIdSaga, id);
}

export default function* () {
  yield takeLatest(PROFILE_PAGE_MOUNT, profilePageMountSaga);
}
