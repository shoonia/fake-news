import {
  put,
  call,
  select,
  takeLatest,
} from 'redux-saga/effects';

import { USER_PAGE_MOUNT } from '../constants';
import { isUsersFetched } from '../selectors';
import { fetchUsers } from '../api';
import { receiveUsers } from '../actions/users';

export function* fetchUsersSaga() {
  const users = yield call(fetchUsers);
  yield put(receiveUsers(users));
}

export function* userPageMountSaga() {
  const isFetched = yield select(isUsersFetched);
  if (!isFetched) {
    yield call(fetchUsersSaga);
  }
}

export default function* () {
  yield takeLatest(USER_PAGE_MOUNT, userPageMountSaga);
}
