import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ROOT_URL } from '../../../constants';

const PostCard = ({ id, title, body }) => (
  <article>
    <h2>
      <Link to={`${ROOT_URL}/posts/${id}`}>
        {title}
      </Link>
    </h2>
    <p>{body}</p>
  </article>
);

PostCard.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
};

export default PostCard;
