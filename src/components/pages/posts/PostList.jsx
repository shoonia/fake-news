import React from 'react';
import PropTypes from 'prop-types';

import PostCard from './PostCard';

const PostList = ({ posts }) => (
  <React.Fragment>
    {posts.map(post => (
      <PostCard key={post.id} {...post} />
    ))}
  </React.Fragment>
);

PostList.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default PostList;
