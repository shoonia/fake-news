import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import HomeView from './home/HomeView';
import { getCurrentPosts } from '../../selectors';
import { setCurrentPosts, clearCurrentPosts } from '../../actions/posts';

class HomePage extends React.Component {
  static propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object).isRequired,
    pageMount: PropTypes.func.isRequired,
    pageUnmount: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { pageMount } = this.props;
    pageMount('1');
  }

  componentWillUnmount() {
    const { pageUnmount } = this.props;
    pageUnmount();
  }

  render() {
    const { posts } = this.props;
    return <HomeView posts={posts} />;
  }
}

const mapStateToProps = state => ({
  posts: getCurrentPosts(state),
});

const mapDispatchToProps = {
  pageMount: setCurrentPosts,
  pageUnmount: clearCurrentPosts,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
