import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getParamsId } from '../../selectors';

class PostPage extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
  };

  state = {};

  render() {
    const { id } = this.props;
    return (
      <div>
        <h1>Post Page</h1>
        <p>{id}</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  id: getParamsId(ownProps),
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(PostPage);
