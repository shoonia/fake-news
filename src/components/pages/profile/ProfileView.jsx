import React from 'react';
import PropTypes from 'prop-types';

const ProfileView = (props) => {
  const {
    name,
    username,
    company,
    website,
    phone,
    address,
  } = props;

  return (
    <main>
      <section>
        <h1>{name}</h1>
        <span>@{username}</span>
        <p>{company.name}</p>
      </section>
      <section>
        <h2>Contacts</h2>
        <ul>
          <li>{website}</li>
          <li>{phone}</li>
        </ul>
      </section>
      <section>
        <h2>Address</h2>
        <ul>
          <li>{address.street}</li>
          <li>{address.city}</li>
          <li>{address.suite}</li>
          <li>{address.zipcode}</li>
        </ul>
      </section>
    </main>
  );
};

ProfileView.propTypes = {
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  website: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  // company
  company: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
  // address
  address: PropTypes.shape({
    street: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    suite: PropTypes.string.isRequired,
    zipcode: PropTypes.string.isRequired,
  }).isRequired,
};

export default ProfileView;
