import React from 'react';
import PropTypes from 'prop-types';

import PostList from '../posts/PostList';

const HomeView = ({ posts }) => (
  <main>
    <h1>Home Page</h1>
    <PostList posts={posts} />
  </main>
);

HomeView.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default HomeView;
