import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getParamsIndex } from '../../selectors';
import { setCurrentPosts, clearCurrentPosts } from '../../actions/posts';

class TapeNewsPage extends React.Component {
  static propTypes = {
    index: PropTypes.string.isRequired,
    pageMount: PropTypes.func.isRequired,
    pageUnmount: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const { index, pageMount } = this.props;
    pageMount(index);
  }

  componentWillUnmount() {
    const { pageUnmount } = this.props;
    pageUnmount();
  }

  render() {
    const { index } = this.props;
    return (
      <div>
        <h1>Tape News Page</h1>
        <p>{index}</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  index: getParamsIndex(ownProps),
});

const mapDispatchToProps = {
  pageMount: setCurrentPosts,
  pageUnmount: clearCurrentPosts,
};

export default connect(mapStateToProps, mapDispatchToProps)(TapeNewsPage);
