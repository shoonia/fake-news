import React from 'react';
import PropTypes from 'prop-types';

import UserItem from './UserItem';

const UserList = ({ users }) => (
  <div role="list">
    {users.map(user => (
      <UserItem key={user.id} {...user} />
    ))}
  </div>
);

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default UserList;
