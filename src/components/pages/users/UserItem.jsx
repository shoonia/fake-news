import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ROOT_URL } from '../../../constants';

const UserItem = ({ id, name, username }) => (
  <div role="listitem">
    <span>{`${name} `}</span>
    <NavLink to={`${ROOT_URL}/users/${id}`}>
      @{username}
    </NavLink>
  </div>
);

UserItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
};

export default UserItem;
