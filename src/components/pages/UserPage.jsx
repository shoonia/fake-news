import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import UserList from './users/UserList';
import { userPageMount } from '../../actions/users';
import { getUsers } from '../../selectors';

class UserPage extends React.Component {
  static propTypes = {
    pageMount: PropTypes.func.isRequired,
    users: PropTypes.arrayOf(PropTypes.object).isRequired,
  };

  componentDidMount() {
    const { pageMount } = this.props;
    pageMount();
  }

  render() {
    const { users } = this.props;
    return <UserList users={users} />;
  }
}

const mapStateToProps = state => ({
  users: getUsers(state),
});

const mapDispatchToProps = {
  pageMount: userPageMount,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
