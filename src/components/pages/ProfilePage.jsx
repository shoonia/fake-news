import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ProfileView from './profile/ProfileView';
import {
  profilePageMount,
  profilePageUnmount,
} from '../../actions/profile';
import {
  getParamsId,
  getProfile,
  isUserLoaded,
  getError,
} from '../../selectors';

class ProfilePage extends React.Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    user: PropTypes.shape({}),
    id: PropTypes.string.isRequired,
    pageMount: PropTypes.func.isRequired,
    pageUnmount: PropTypes.func.isRequired,
  };

  static defaultProps = {
    user: {},
  };

  componentDidMount() {
    const { id, pageMount } = this.props;
    pageMount(parseInt(id, 10));
  }

  componentWillUnmount() {
    const { pageUnmount } = this.props;
    pageUnmount();
  }

  render() {
    const { isLoaded, error, user } = this.props;

    if (!isLoaded) {
      return null;
    }

    if (error !== '') {
      return null;
    }

    return <ProfileView {...user} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  isLoaded: isUserLoaded(state),
  error: getError(state),
  user: getProfile(state),
  id: getParamsId(ownProps),
});

const mapDispatchToProps = {
  pageMount: profilePageMount,
  pageUnmount: profilePageUnmount,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
