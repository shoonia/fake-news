import Loadable from 'react-loadable';

import { ROOT_URL } from '../constants';

const loading = () => null;

const HomePage = Loadable({
  loader: () => import('./pages/HomePage' /* webpackChunkName: "HomePage" */),
  loading,
});

const UserPage = Loadable({
  loader: () => import('./pages/UserPage' /* webpackChunkName: "UserPage" */),
  loading,
});

const ProfilePage = Loadable({
  loader: () => import('./pages/ProfilePage' /* webpackChunkName: "ProfilePage" */),
  loading,
});

const PostPage = Loadable({
  loader: () => import('./pages/PostPage' /* webpackChunkName: "PostPage" */),
  loading,
});

const TapeNewsPage = Loadable({
  loader: () => import('./pages/TapeNewsPage' /* webpackChunkName: "TapeNewsPage" */),
  loading,
});

const NotFound = Loadable({
  loader: () => import('./not-found/NotFound' /* webpackChunkName: "NotFound" */),
  loading,
});

export default [
  {
    path: `${ROOT_URL}/`,
    exact: true,
    component: HomePage,
  },
  {
    path: `${ROOT_URL}/users`,
    exact: true,
    component: UserPage,
  },
  {
    path: `${ROOT_URL}/users/:id`,
    exact: true,
    component: ProfilePage,
  },
  {
    path: `${ROOT_URL}/posts/:id`,
    exact: true,
    component: PostPage,
  },
  {
    path: `${ROOT_URL}/posts/page/:index`,
    exact: true,
    component: TapeNewsPage,
  },
  {
    path: '*',
    component: NotFound,
  },
];
