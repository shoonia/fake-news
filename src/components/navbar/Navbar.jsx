import React from 'react';
import { createPortal } from 'react-dom';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ROOT_URL } from '../../constants';

const Navbar = ({ root }) => createPortal(
  <ul>
    <li>
      <NavLink to={`${ROOT_URL}/`}>
        Home
      </NavLink>
    </li>
    <li>
      <NavLink to={`${ROOT_URL}/users`}>
        Users
      </NavLink>
    </li>
    <li>
      <NavLink to={`${ROOT_URL}/posts/page/1`}>
        Posts
      </NavLink>
    </li>
  </ul>,
  document.getElementById(root),
);

Navbar.propTypes = {
  root: PropTypes.string.isRequired,
};

export default Navbar;
