import {
  PROFILE_PAGE_UNMOUNT,
  RECEIVE_CURRENT_USER,
  PROFILE_ERROR,
} from '../constants';

const initialState = {
  isLoaded: false,
  error: '',
  item: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PROFILE_PAGE_UNMOUNT:
      return {
        ...initialState,
      };

    case RECEIVE_CURRENT_USER:
      return {
        isLoaded: true,
        error: '',
        item: action.user,
      };

    case PROFILE_ERROR:
      return {
        isLoaded: true,
        error: action.error,
      };

    default:
      return state;
  }
};
