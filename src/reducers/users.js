import { RECEIVE_USERS } from '../constants';

const initialState = {
  isFetched: false,
  items: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_USERS:
      return {
        isFetched: true,
        items: action.users,
      };

    default:
      return state;
  }
};
