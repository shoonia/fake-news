import { combineReducers } from 'redux';

import users from './users';
import profile from './profile';
import posts from './posts';

export default combineReducers({
  users,
  profile,
  posts,
});
