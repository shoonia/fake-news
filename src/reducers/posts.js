import {
  PUT_CURRENT_POSTS,
  ADD_POSTS_TO_PAGE_LIST,
  CLEAR_CURRENT_POSTS,
} from '../constants';

const initialState = {
  currentPosts: [],
  pageList: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PUT_CURRENT_POSTS:
      return {
        ...state,
        currentPosts: action.posts,
      };

    case CLEAR_CURRENT_POSTS:
      return {
        ...state,
        currentPosts: [],
      };

    case ADD_POSTS_TO_PAGE_LIST: {
      const pageList = {
        ...state.pageList,
        [action.index]: action.posts,
      };
      return {
        ...state,
        currentPosts: action.posts,
        pageList,
      };
    }

    default:
      return state;
  }
};
