import {
  SET_CURRENT_POSTS,
  CLEAR_CURRENT_POSTS,
  PUT_CURRENT_POSTS,
  ADD_POSTS_TO_PAGE_LIST,
} from '../constants';

export const setCurrentPosts = index => ({
  type: SET_CURRENT_POSTS,
  index,
});

export const putCurrentPosts = posts => ({
  type: PUT_CURRENT_POSTS,
  posts,
});

export const clearCurrentPosts = () => ({
  type: CLEAR_CURRENT_POSTS,
});

export const addPostsToPageList = (index, posts) => ({
  type: ADD_POSTS_TO_PAGE_LIST,
  index,
  posts,
});
