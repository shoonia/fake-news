import { USER_PAGE_MOUNT, RECEIVE_USERS } from '../constants';

export const userPageMount = () => ({
  type: USER_PAGE_MOUNT,
});

export const receiveUsers = users => ({
  type: RECEIVE_USERS,
  users,
});
