import {
  PROFILE_PAGE_MOUNT,
  PROFILE_PAGE_UNMOUNT,
  RECEIVE_CURRENT_USER,
  PROFILE_ERROR,
} from '../constants';

export const profilePageMount = id => ({
  type: PROFILE_PAGE_MOUNT,
  id,
});

export const profilePageUnmount = () => ({
  type: PROFILE_PAGE_UNMOUNT,
});

export const receiveCurrentUser = user => ({
  type: RECEIVE_CURRENT_USER,
  user,
});

export const profileError = error => ({
  type: PROFILE_ERROR,
  error,
});
