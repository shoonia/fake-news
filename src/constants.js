// global:
export const ROOT_URL = (process.env.NODE_ENV === 'development')
  ? ''
  : '/fake-news';

// redux action types:
export const USER_PAGE_MOUNT = 'USER_PAGE_MOUNT';

export const RECEIVE_USERS = 'RECEIVE_USERS';
export const RECEIVE_CURRENT_USER = 'RECEIVE_CURRENT_USER';

export const PROFILE_PAGE_MOUNT = 'PROFILE_PAGE_MOUNT';
export const PROFILE_PAGE_UNMOUNT = 'PROFILE_PAGE_UNMOUNT';
export const PROFILE_ERROR = 'PROFILE_ERROR';

export const SET_CURRENT_POSTS = 'SET_CURRENT_POSTS';
export const PUT_CURRENT_POSTS = 'PUT_CURRENT_POSTS';
export const CLEAR_CURRENT_POSTS = 'CLEAR_CURRENT_POSTS';
export const ADD_POSTS_TO_PAGE_LIST = 'ADD_POSTS_TO_PAGE_LIST';
